local AceLocale = LibStub("AceLocale-3.0")
local L = AceLocale:NewLocale("MeetingStone", "esES") or AceLocale:NewLocale("MeetingStone", "esMX")
if not L then return end
--@localization(locale="esES", format="lua_additive_table", same-key-is-true=true, handle-subnamespaces="concat")@
