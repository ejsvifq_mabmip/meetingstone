
local WIDGET, VERSION = 'InTabButton', 1

local GUI = LibStub('NetEaseGUI-2.0')
local InTabButton = GUI:NewClass(WIDGET, GUI:GetClass('ItemButton'), VERSION)
if not InTabButton then
    return
end

function InTabButton:Constructor()
    self:SetHeight(22)
    self:SetWidth(100)

    self:SetNormalFontObject('GameFontNormalSmall')
    self:SetDisabledFontObject('GameFontHighlightSmall')
    self:SetHighlightFontObject('GameFontHighlightSmall')

    self:SetHighlightTexture([[Interface\DialogFrame\UI-DialogBox-Background-Dark]], 'ADD')
    self:GetHighlightTexture():SetHeight(32)
    self:GetHighlightTexture():SetPoint('BOTTOM', 2, -8)
end

function InTabButton:SetStatus(status)
    if status == 'SELECTED' then
        self:Disable()
        self:SetDisabledFontObject('GameFontHighlightSmall')
    elseif status == 'NORMAL' then
        self:Enable()
    elseif status == 'DISABLED' then
        self:Disable()
        self:SetDisabledFontObject('GameFontDisableSmall')
    end

    self:SetWidth(self:GetTextWidth() + 30)
end
