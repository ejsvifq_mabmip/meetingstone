
local WIDGET, VERSION = 'BottomTabButton', 2

local GUI = LibStub('NetEaseGUI-2.0')
local BottomTabButton = GUI:NewClass(WIDGET, GUI:GetClass('TabItemButton'), VERSION)
if not BottomTabButton then
    return
end

function BottomTabButton:Constructor()
    self:SetSize(10, 32)


    self:SetFontString(self:CreateFontString(nil, 'OVERLAY'))
    self:SetNormalFontObject('GameFontNormalSmall')
    self:SetDisabledFontObject('GameFontHighlightSmall')
    self:SetHighlightFontObject('GameFontHighlightSmall')

    self:SetHighlightTexture([[Interface\PaperDollInfoFrame\UI-Character-Tab-RealHighlight]], 'ADD')
    self:GetHighlightTexture():ClearAllPoints()
    self:GetHighlightTexture():SetPoint('TOPLEFT', 3, 5)
    self:GetHighlightTexture():SetPoint('BOTTOMRIGHT', -3, 0)
    self:GetHighlightTexture():SetTexCoord(0.1, 0.9, 0, 1)
end

function BottomTabButton:SetStatus(status)
    if status == 'SELECTED' then
        self:Disable()
        self:SetDisabledFontObject('GameFontHighlightSmall')
        self:GetFontString():SetPoint('CENTER', 0, -3)
    elseif status == 'NORMAL' then
        self:Enable()
        self:GetFontString():SetPoint('CENTER', 0, 2)
    elseif status == 'DISABLED' then
        self:Disable()
        self:SetDisabledFontObject('GameFontDisableSmall')
        self:GetFontString():SetPoint('CENTER', 0, 2)
    end

    self:SetWidth(self:GetTextWidth() + 30)
end
