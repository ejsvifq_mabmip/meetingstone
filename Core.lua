local AceAddon = LibStub('AceAddon-3.0')
local LookingForGroup = AceAddon:GetAddon("LookingForGroup")
local MS = LookingForGroup:NewModule("MS","AceHook-3.0","AceEvent-3.0")
local MeetingStone = AceAddon:GetAddon("MeetingStone")
local MeetingStone_MainPanel = MeetingStone:GetModule('MainPanel')
local MeetingStone_BrowsePanel = MeetingStone:GetModule('BrowsePanel')
local MeetingStone_Profile = MeetingStone:GetModule('Profile')
local MeetingStone_Activity = MeetingStone:GetClass('Activity')
local NetEaseSocketMiddleware = LibStub('NetEaseSocketMiddleware-2.0')
local LfgService = MeetingStone:GetModule('LfgService')
--MS:RawHook(NetEaseSocketMiddleware,"Send",nop)
function MS:OnInitialize()
	LfgService:UnregisterEvent("LFG_LIST_SEARCH_RESULTS_RECEIVED")
	LfgService:UnregisterEvent("LFG_LIST_SEARCH_FAILED")
	self:RawHook(MeetingStone_BrowsePanel,"EndSet",function(self)
		self.inSet = nil
	end)
	self:RawHook(MeetingStone_BrowsePanel,"Search",nop)
	self:RawHook(MeetingStone_BrowsePanel,"DoSearch")
	self:RegisterMessage("MEETINGSTONE_OPEN","DoSearch")
--	self:RawHook(MeetingStone_MainPanel,"OpenActivityTooltip")
end

function MS:OnEnable()
end

local LFG_OPT=AceAddon:GetAddon("LookingForGroup_Options" )
LFG_OPT.RegisterSimpleFilter("ms",function(info,profile,activityId)
	if info.activityId ~= activityId then
		return 1
	end
end,function(profile)
	local activityItem = MeetingStone_BrowsePanel.ActivityDropdown:GetItem()
	if not activityItem then
		return
	end
	if MeetingStone_BrowsePanel.SearchBox:GetText() == activityItem.fullName then
		return activityId
	end
end)
local function searchingui()
	table.wipe(LfgService.activityList)
	table.wipe(LfgService.activityHash)
	table.wipe(LfgService.activityRemoved)
    MeetingStone_BrowsePanel.SearchingBlocker:Show()
    MeetingStone_BrowsePanel.NoResultBlocker:Hide()
    MeetingStone_BrowsePanel.RefreshButton:Disable()
    MeetingStone_BrowsePanel.RefreshFilterButton:Disable()
end


local function updateui(isFailed,resultList)
	table.wipe(LfgService.activityList)
	table.wipe(LfgService.activityHash)
	table.wipe(LfgService.activityRemoved)

	local applications = C_LFGList.GetApplications()

	for _, id in ipairs(applications) do
		LfgService:CacheActivity(id)
	end
	if resultList then
		for _, id in ipairs(resultList) do
			LfgService:CacheActivity(id)
		end
	end
	local count = LfgService:GetActivityCount()
    MeetingStone_BrowsePanel.SearchingBlocker:Hide()
    MeetingStone_BrowsePanel.RefreshButton:Enable()
    MeetingStone_BrowsePanel.RefreshFilterButton:Enable()
	MeetingStone_BrowsePanel.NoResultBlocker.Button:SetShown(not isFailed)
	MeetingStone_BrowsePanel.ActivityList:Refresh()
	if isFailed == 2 then
		MeetingStone_BrowsePanel.NoResultBlocker.Label:SetText("")
		MS:SendMessage("MEETINGSTONE_DataBroker_Cleanup")
	else
		MeetingStone_BrowsePanel.NoResultBlocker:SetShown(count == 0)
		MeetingStone_BrowsePanel.NoResultBlocker.Label:SetText(isFailed and [[|TInterface\DialogFrame\UI-Dialog-Icon-AlertNew:30|t  ]] .. LFG_LIST_SEARCH_FAILED or LFG_LIST_NO_RESULTS_FOUND)
		MS:SendMessage('MEETINGSTONE_ACTIVITIES_COUNT_UPDATED', count)
	end
end

function MS.search(filter_options,category,filters,preferredfilters)
	local lfg_profile = LookingForGroup.db.profile
	local profile = LFG_OPT.db.profile
	local hardware = lfg_profile.hardware
	local current = coroutine.running()
	local function resume()
		LookingForGroup.resume(current)
	end
	local unsecure_state
	local yd = 0
	local function resume_1()
		LookingForGroup.resume(current,1)
	end
	MS:RegisterMessage("LFG_CORE_FINALIZER",resume)
	MS:RegisterMessage("LFG_ICON_MIDDLE_CLICK",resume)
	local function update_search_result(event,id)
		LfgService:UpdateActivity(id)
		MS:SendMessage('MEETINGSTONE_ACTIVITIES_RESULT_UPDATED')
	end
	MS:RegisterEvent("LFG_LIST_SEARCH_RESULT_UPDATED",update_search_result)
	MS:RegisterEvent("LFG_LIST_APPLICATION_STATUS_UPDATED",update_search_result)
	MeetingStone_BrowsePanel.SearchingBlocker:Hide()
	local count, results
	local timer
	local pending
	local C_LFGList = C_LFGList
	local bts = {}
	local tb = {}
	local error_code = 2
	while true do
		if type(yd)~="number" then
			break
		end
		local elapse_time_start = GetTime()
		local error_msg
		if not unsecure_state then
			if yd == 0 then
				searchingui()					
			end
			LFG_OPT.ExecuteSearchPattern(filter_options)
			count, results = LookingForGroup.Search(category,filters,preferredfilters)
			if count == 0 then
				if hardware then
					error_code=results and true or false
					break
				end
				updateui(results and true or false)
			end
			unsecure_state=hardware
		end
		repeat
		if yd < 2 then
			local pause_duration = 10
			if results then
				local ftrs = LFG_OPT.ExecuteFilter(bts,tb,results,filter_options,yd == 0)
				updateui(false,ftrs)
				pause_duration = pause_duration+#ftrs
			end
			if LFG_OPT.Background_Timer then
				LFG_OPT.Background_Timer:Cancel()
			end
			LFG_OPT.Background_Timer = C_Timer.NewTicker(pause_duration,resume_1)
			repeat
				yd = coroutine.yield()
				local item = MeetingStone_BrowsePanel.ActivityList:GetSelectedItem()
				if item == nil then
					break
				end
			until yd ~= 1
			LFG_OPT.Background_Timer:Cancel()
			LFG_OPT.Background_Timer = nil
		end
		until true
	end
	C_LFGList.ClearSearchResults()
	if LFG_OPT.Background_Timer then
		LFG_OPT.Background_Timer:Cancel()
		LFG_OPT.Background_Timer = nil
	end
	MS:UnregisterMessage("LFG_CORE_FINALIZER")
	MS:UnregisterMessage("LFG_ICON_MIDDLE_CLICK")
	MS:UnregisterEvent("LFG_LIST_SEARCH_RESULT_UPDATED")
	MS:UnregisterEvent("LFG_LIST_APPLICATION_STATUS_UPDATED")
	updateui(error_code)
end

function MS:DoSearch()
	if MeetingStone_BrowsePanel:InSet() then
		return
	end
	if MeetingStone_BrowsePanel.searchedInFrame then
		return
	end
	local activityItem = MeetingStone_BrowsePanel.ActivityDropdown:GetItem()
	if not activityItem then
		return
	end
	local categoryId = activityItem.categoryId
	local db = LFG_OPT.db
	local a = db.profile.a
	a.category = categoryId
	a.group = nil
	a.activity = nil
	local baseFilter = activityItem.baseFilter
	local searchCode = activityItem.value
	local activityId = activityItem.activityId
	if not categoryId or not MeetingStone_MainPanel:IsVisible() then
		return
	end
	local filters = LE_LFG_LIST_FILTER_RECOMMENDED
	if MeetingStone_BrowsePanel.SearchBox:GetText() ~= activityItem.fullName then
		activityId = nil
	end
	a.activity = activityId
	if activityId then
		local fullName, shortName, categoryID, groupID, itemLevel, ffilters, minLevel, maxPlayers, displayType
		filters = ffilters
		a.category = categoryID
		a.group = groupID
		a.activity = activityId
	end
	MeetingStone_Profile:SetLastSearchCode(searchCode)
	coroutine.wrap(MS.search)({"spam","ms"},categoryId,filters,0)
end
--[[
function MS:OpenActivityTooltip(MainPanel,activity)
	if GameTooltip:GetOwner() == MainPanel then
		MainPanel:GetScript("OnLeave")()
	end
	MS:RegisterMessage("LFG_CORE_FINALIZER",function()
		if GameTooltip:GetOwner() == MainPanel then
			MainPanel:GetScript("OnLeave")()
		end
		MS:UnregisterMessage("LFG_CORE_FINALIZER")
	end)
	GameTooltip:SetOwner(MainPanel, 'ANCHOR_NONE')
	GameTooltip:SetPoint('TOPLEFT', MainPanel, 'TOPRIGHT', 1, -10)
	coroutine.wrap(MS.lfg_opt().search_result_tooltip_coroutine)(MainPanel,activity:GetID())
end]]
