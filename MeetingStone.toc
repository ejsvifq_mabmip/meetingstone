## Interface: 80205
## Title: Meeting Stone
## Title-zhCN: 集合石
## Notes-zhCN: 帮助您更好的组建队伍 (该集合石插件非官方版本，内部使用LFG插件的引擎)
## Version: LFG @project-version@
## Author: cqwrteur
## Dependencies: LookingForGroup_Options
## SavedVariables: MEETINGSTONE_UI_DB
## SavedVariablesPerCharacter: MEETINGSTONE_CHARACTER_DB
## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed
## X-WoWBox-Ver: 1

#@no-lib-strip@
Libs\AceTimer-3.0\AceTimer-3.0.xml
Libs\AceBucket-3.0\AceBucket-3.0.xml
Libs\AceComm-3.0\AceComm-3.0.xml
Libs\AceSerializer-3.0\AceSerializer-3.0.xml
#@end-no-lib-strip@

Localization\Localization.xml
Load.xml
Bindings.lua